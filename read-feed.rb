require 'rss'
require 'time'

url = 'https://milosophical.me/authors/michael-lockhart.xml'
begin_blogs = "<!---BEGIN-BLOG--->\n"
end_blogs = "<!---END-BLOG--->"
blogs_regex = /<!---BEGIN-BLOG--->(.*)<!---END-BLOG--->/im
README = "README.md"

# Get the blogs

feed_string = begin_blogs
feed_string += "|Post | Published|\n"
feed_string += "|--|--|\n"
URI.open(url) do |rss|
  feed = RSS::Parser.parse(rss)
  feed.items.each do |item|
    blog_item = "|[#{item.title}](#{item.link}) | #{item.pubDate.iso8601}|\n"
    blog_item = blog_item.gsub('http://','https://')
    feed_string += blog_item 
    puts blog_item
  end
end
feed_string += end_blogs

# Load the README
readme_file = File.open(README)
readme_buffer = readme_file.read
readme_file.close

# Insert the blogs
File.write(README, readme_buffer.gsub(blogs_regex, feed_string))
