# 👋 G'day, I'm Mike Lockhart 

Computer generalist, percussionist, humanist, and dad. Sometimes also a computer-percussionist.
I make computers work for people, and I'm lucky enough to do it for [my day job](https://gitlab.com/mlockhart).
I like to help all kinds of people with technology stuff. 

I enjoy coding, though I wouldn't call myself a hacker or even a great programmer, more a tool-smith for my work and hobbies. I appreciate the _craft_ in coding and love to look over and admire other people's code.

- 📇 My online business card is https://about.me/mikelockhart
- 🐘 <a rel="me" href="https://mastodon.au/@milohax">Mastodon AU</a>, <a rel="me" href="https://hachyderm.io/@milohax">Hachyderm.io</a>
- 💻 [Public SSH keys on GitLab.com](https://gitlab.com/milohax.keys)
- 🔐 [Public PGP key 3CCA2E6EBCBE8795](https://keybase.io/sinewalker/key.asc)
- 🐙 I also have [a GitHub account](https://github.com/sinewalker)

Do get in touch with me for meet-ups online and around Hobart (my timezone: UTC+10).

---

This user account is for non-work projects and contributions.

Most of the things here are the usual: blog and other web site projects, hacks to learn technologies, dotfiles, shell scripts and so on. I try to keep my work projects separate, though a lot of what I do for money is also Open Source these days. You'll find mostly incomplete work, but I do try to keep very good engineering notes, it's my most closely held [Rule of computing](https://milosophical.me/pg/4-bit-rules.html): Rule 4: [If you're exploring, keep a notebook or journal](https://milosophical.me/blog/2015/4-bit-rules-of-computing-part-1.html). GitLab is the ideal software engineer's lab book.

## 🧪 Featured projects

|  [![milosophical-me](https://gitlab.com/uploads/-/system/project/avatar/22997301/heart-trans.png?width=128px)<br/>milosophical-me](https://gitlab.com/milohax/milosophical-me) <br/> My personal web  |  [![hax](https://gitlab.com/uploads/-/system/project/avatar/23332426/k20394137.jpg?width=128px)<br/>Lab Book](https://gitlab.com/milohax/hax) <br/> Gathers all my geek things  | [![milohax.net](https://gitlab.com/uploads/-/system/project/avatar/20435372/heart-trans.png?width=128px)<br/> milohax.net/web/tech-wiki](https://gitlab.com/milohax-net/web/tech-wiki) <br/> Technical wiki  | [![web-hax](https://gitlab.com/uploads/-/system/project/avatar/25619672/matrix-animated-image.gif?width=128px)<br/> milohax.net/web-hax](https://gitlab.com/milohax-net/web-hax) <br/>Collecton of web hacks |
| -- | -- | -- | -- |

## 📦 Groups

| [![milohax-net](https://gitlab.com/uploads/-/system/group/avatar/8767399/tao.jpg?width=128px) <br/> milohax.net](https://gitlab.com/milohax-net) <br/> Programming projects shared on the Net | [![milohax-net/web](https://gitlab.com/uploads/-/system/group/avatar/11733707/web-browser.png?width=128px) <br/> milohax.net/web](https://gitlab.com/milohax-net/web) <br/> Web projects published to milohax.net domain | [![milohax-net/radix](https://gitlab.com/uploads/-/system/group/avatar/11733837/Screen_Shot_2021-04-16_at_07.18.05.png?width=128px) <br/> milohax.net/radix](https://gitlab.com/milohax-net/radix) <br/> Projects for setting up my computer environments |
| -- | -- | -- |
| [![milohax-net/fish](https://gitlab.com/uploads/-/system/group/avatar/12616431/fish.png?width=128px) <br/> milohax-net/fish](https://gitlab.com/milohax-net/fish) <br/> Plugin modules for the fish shell | [![milohax-net/hub](https://gitlab.com/uploads/-/system/group/avatar/12521410/icon-github.png?width=128px) <br/> milohax.net/hub](https://gitlab.com/milohax-hub) <br/> Projects from GitHub mirrored as forks. | [![milohax-net/attic](https://gitlab.com/uploads/-/system/group/avatar/12086269/Screen_Shot_2021-05-16_at_17.18.46.png?width=128px) <br/> milohax.net/attic](https://gitlab.com/milohax-attic) <br/> Dusty old projects are kept in here, where they may bitrot in peace. |


[All personal groups](https://gitlab.com/users/milohax/groups)

## 📔 Latest blog posts

<!---BEGIN-BLOG--->
|Post | Published|
|--|--|
|[Saving and Restoring GCP instance snapshots](https://milosophical.me/blog/2025/gcp-snapshot.html) | 2025-03-02T20:34:29+00:00|
|[4-bit Rules of Computing, Part 5](https://milosophical.me/blog/2024/4-bits-part5.html) | 2024-12-28T22:29:16+00:00|
|[Password databases: updating master key](https://milosophical.me/blog/2023/password-store-2.html) | 2023-05-28T10:58:56+00:00|
|[Code Clubbing 2023](https://milosophical.me/blog/2023/codeclub-0.html) | 2023-03-17T22:21:30+00:00|
|[GitLab Community Meetup February](https://milosophical.me/blog/2023/gitlab-meetup-february.html) | 2023-03-02T10:31:19+00:00|
|[Learning DVCS Workflow - 3](https://milosophical.me/blog/2023/learning-git-workflow-3.html) | 2023-03-01T07:45:28+00:00|
|[Tech projects at start of 2023](https://milosophical.me/blog/2023/hackng-2023.html) | 2023-01-22T06:51:08+00:00|
|[Control Characters](https://milosophical.me/blog/2022/control-characters.html) | 2022-02-19T09:55:01+00:00|
|[GitLab Profile Multi-project Pipeline](https://milosophical.me/blog/2021/gitlab-multi-project-pipeline.html) | 2021-12-17T16:52:40+00:00|
|[Personalised GitLab Profile page](https://milosophical.me/blog/2021/gitlab-profile.html) | 2021-12-13T05:19:21+00:00|
<!---END-BLOG--->

